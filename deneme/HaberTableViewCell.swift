//
//  HaberTableViewCell.swift
//  deneme
//
//  Created by FONET BİLGİ TEKNOLOJİ on 17.01.2019.
//  Copyright © 2019 Ytam. All rights reserved.
//

import UIKit

class HaberTableViewCell: UITableViewCell {
    @IBOutlet weak var haberPhoto: UIImageView!
    @IBOutlet weak var haberBaslik: UILabel!
    @IBOutlet weak var haberTarih: UILabel!
    @IBOutlet weak var haberYazar: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
