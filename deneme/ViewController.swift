//
//  ViewController.swift
//  deneme
//
//  Created by FONET BİLGİ TEKNOLOJİ on 17.01.2019.
//  Copyright © 2019 Ytam. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    
    @IBOutlet weak var tableView: UITableView!
    var ulkeler: [String] = ["Türkiye", "Almanya","Rusya","Mısır","Portekiz","KKTC","Azerbaycan", "Rusya","Mısır","Portekiz","KKTC","Azerbaycan"]
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
         return ulkeler.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell:UITableViewCell = self.tableView.dequeueReusableCell(withIdentifier: "cell") as! HaberTableViewCell
        
        cell.textLabel?.text = self.ulkeler[indexPath.row]
        
        return cell
    }
    

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }


}

